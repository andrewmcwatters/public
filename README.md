# /public [![Build Status](https://travis-ci.org/andrewmcwatters/public.svg?branch=master)](https://travis-ci.org/andrewmcwatters/public) [![Codacy Badge](https://www.codacy.com/project/badge/6c3d3527a7ba44c8b4ee7acc239870a5)](https://www.codacy.com/public/andrewmcwatters/public)

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
