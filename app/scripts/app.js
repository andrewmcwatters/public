'use strict';

/**
 * @ngdoc overview
 * @name publicApp
 * @description
 * # publicApp
 *
 * Main module of the application.
 */
angular
  .module('publicApp', [
    'ui.router',
    'ui.odometer',
    'scrollto',
    'btford.socket-io',
    'angular-loading-bar'
  ])

  /**
   * Configure states.
   */

  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/about', '/');
    $urlRouterProvider.otherwise('/404');

    $urlRouterProvider.rule(function($injector, $location) {
      var path = $location.path();
      if (path !== '/' && path.slice(-1) === '/') {
        $location.replace().path(path.slice(0, -1));
      }
    });

    $stateProvider
      .state('about', {
        url: '/',
        templateUrl: 'views/main.html'
      })
      .state('wiwo', {
        url: '/wiwo',
        templateUrl: 'views/wiwo.html',
        controller: 'WiwoCtrl'
      })
      .state('portfolio', {
        url: '/portfolio',
        templateUrl: 'views/portfolio.html',
        controller: 'PortfolioCtrl'
      })
      .state('theme', {
        url: '/theme',
        templateUrl: 'views/theme.html'
      })
      .state('404', {
        url: '/404',
        templateUrl: 'views/404.html'
      })
      .state('status', {
        url: '/status',
        templateUrl: 'views/status.html',
        controller: 'StatusCtrl'
      });
  })

  /**
   * Configure URL matcher.
   */

  .config(function ($urlMatcherFactoryProvider) {
    $urlMatcherFactoryProvider.strictMode(false);
  })

  /**
   * Configure application deep linking.
   */

  .config(function ($locationProvider) {
    $locationProvider
      .html5Mode(true)
      .hashPrefix('!');
  })

  /**
   * Decorate controllers with route name.
   */

  .config(function ($provide) {
    $provide.decorator('$controller', function ($location, $delegate) {
      return function(constructor, locals, later, indent) {
        function updateRouteName() {
          var path      = $location.path() || '/';
          var routeName = path.replace('/', '');
          if (routeName === '') {
         // routeName = 'main';
            routeName = 'about';
          }

          locals.$scope.routeName = routeName;
        } updateRouteName();

        locals.$scope.$on('$viewContentLoaded', updateRouteName);
        return $delegate(constructor, locals, later, indent);
      };
    });
  })

  /**
   * Instantiate FastClick.
   */

  .run(function($window, $document) {
    var FastClick = $window.FastClick;
    FastClick.attach($document[0].body);
  })

  /**
   * Default ease.
   */

  .run(function($window) {
    var TweenLite = $window.TweenLite;
    var Quint     = $window.Quint;
    TweenLite.defaultEase = Quint.easeOut;
  });
